
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 07/30/2015 22:33:55
-- Generated from EDMX file: C:\Users\Camilo\Desktop\Paola\Sw 3\FinishSoftware\FinishSoftware\Model1.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [BDFinish];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------


-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'clienteSet'
CREATE TABLE [dbo].[clienteSet] (
    [codigo_cliente] int IDENTITY(1,1) NOT NULL,
    [nombres_cliente] nvarchar(max)  NOT NULL,
    [apellidos_cliente] nvarchar(max)  NOT NULL,
    [celular] nvarchar(max)  NOT NULL,
    [direccion] nvarchar(max)  NOT NULL,
    [email] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'canchaSet'
CREATE TABLE [dbo].[canchaSet] (
    [codigo_cancha] int IDENTITY(1,1) NOT NULL,
    [nombre_cancha] nvarchar(max)  NOT NULL,
    [caracteristicas] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'usuarioSet'
CREATE TABLE [dbo].[usuarioSet] (
    [codigo_usuario] int IDENTITY(1,1) NOT NULL,
    [username] nvarchar(max)  NOT NULL,
    [password] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'reservaSet'
CREATE TABLE [dbo].[reservaSet] (
    [codigo_reserva] int IDENTITY(1,1) NOT NULL,
    [fecha_creacion] nvarchar(max)  NOT NULL,
    [fecha_reserva] nvarchar(max)  NOT NULL,
    [valor_reserva] nvarchar(max)  NOT NULL,
    [cancha_codigo_cancha] int  NOT NULL,
    [cliente_codigo_cliente] int  NOT NULL,
    [horario_codigo_horario] int  NOT NULL
);
GO

-- Creating table 'horarioSet'
CREATE TABLE [dbo].[horarioSet] (
    [codigo_horario] int IDENTITY(1,1) NOT NULL,
    [hora_inicio] nvarchar(max)  NOT NULL,
    [hora_fin] nvarchar(max)  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [codigo_cliente] in table 'clienteSet'
ALTER TABLE [dbo].[clienteSet]
ADD CONSTRAINT [PK_clienteSet]
    PRIMARY KEY CLUSTERED ([codigo_cliente] ASC);
GO

-- Creating primary key on [codigo_cancha] in table 'canchaSet'
ALTER TABLE [dbo].[canchaSet]
ADD CONSTRAINT [PK_canchaSet]
    PRIMARY KEY CLUSTERED ([codigo_cancha] ASC);
GO

-- Creating primary key on [codigo_usuario] in table 'usuarioSet'
ALTER TABLE [dbo].[usuarioSet]
ADD CONSTRAINT [PK_usuarioSet]
    PRIMARY KEY CLUSTERED ([codigo_usuario] ASC);
GO

-- Creating primary key on [codigo_reserva] in table 'reservaSet'
ALTER TABLE [dbo].[reservaSet]
ADD CONSTRAINT [PK_reservaSet]
    PRIMARY KEY CLUSTERED ([codigo_reserva] ASC);
GO

-- Creating primary key on [codigo_horario] in table 'horarioSet'
ALTER TABLE [dbo].[horarioSet]
ADD CONSTRAINT [PK_horarioSet]
    PRIMARY KEY CLUSTERED ([codigo_horario] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [cliente_codigo_cliente] in table 'reservaSet'
ALTER TABLE [dbo].[reservaSet]
ADD CONSTRAINT [FK_clientereserva]
    FOREIGN KEY ([cliente_codigo_cliente])
    REFERENCES [dbo].[clienteSet]
        ([codigo_cliente])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_clientereserva'
CREATE INDEX [IX_FK_clientereserva]
ON [dbo].[reservaSet]
    ([cliente_codigo_cliente]);
GO

-- Creating foreign key on [cancha_codigo_cancha] in table 'reservaSet'
ALTER TABLE [dbo].[reservaSet]
ADD CONSTRAINT [FK_canchareserva]
    FOREIGN KEY ([cancha_codigo_cancha])
    REFERENCES [dbo].[canchaSet]
        ([codigo_cancha])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_canchareserva'
CREATE INDEX [IX_FK_canchareserva]
ON [dbo].[reservaSet]
    ([cancha_codigo_cancha]);
GO

-- Creating foreign key on [horario_codigo_horario] in table 'reservaSet'
ALTER TABLE [dbo].[reservaSet]
ADD CONSTRAINT [FK_horarioreserva]
    FOREIGN KEY ([horario_codigo_horario])
    REFERENCES [dbo].[horarioSet]
        ([codigo_horario])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_horarioreserva'
CREATE INDEX [IX_FK_horarioreserva]
ON [dbo].[reservaSet]
    ([horario_codigo_horario]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------