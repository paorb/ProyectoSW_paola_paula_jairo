//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FinishSoftware
{
    using System;
    using System.Collections.Generic;
    
    public partial class cancha
    {
        public cancha()
        {
            this.reserva = new HashSet<reserva>();
        }
    
        public int codigo_cancha { get; set; }
        public string nombre_cancha { get; set; }
        public string caracteristicas { get; set; }
    
        public virtual ICollection<reserva> reserva { get; set; }
    }
}
