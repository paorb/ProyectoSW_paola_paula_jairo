//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FinishSoftware
{
    using System;
    using System.Collections.Generic;
    
    public partial class cliente
    {
        public cliente()
        {
            this.reserva = new HashSet<reserva>();
        }
    
        public int codigo_cliente { get; set; }
        public string nombres_cliente { get; set; }
        public string apellidos_cliente { get; set; }
        public string celular { get; set; }
        public string direccion { get; set; }
        public string email { get; set; }
    
        public virtual ICollection<reserva> reserva { get; set; }
    }
}
