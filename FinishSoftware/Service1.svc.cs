﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace FinishSoftware
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "Service1" en el código, en svc y en el archivo de configuración.
    // NOTE: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione Service1.svc o Service1.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class Service1 : IService1
    {
        public List<cancha> findAll()
        {
            using (Model1Container can = new Model1Container())
            {
                return can.canchaSet.ToList();

            }
        }
        public cancha find(string codigo)
        {
            using (Model1Container can = new Model1Container())
            {
                int ncodigo = Convert.ToInt32(codigo);
                return can.canchaSet.Where(ca => ca.codigo_cancha == ncodigo).Select(ca => new cancha
                {
                    codigo_cancha = ca.codigo_cancha,
                    nombre_cancha = ca.nombre_cancha,
                    caracteristicas = ca.caracteristicas,
                }).First();

            };
        }
        public bool create(cancha cancha)
        {
            using (Model1Container can = new Model1Container())
            {
                try
                {
                    cancha ca = new cancha();
                    ca.nombre_cancha = cancha.nombre_cancha;
                    ca.caracteristicas = cancha.caracteristicas;
                    can.canchaSet.Add(ca);
                    can.SaveChanges();
                    return true;
                }
                catch
                {
                    return false;
                }

            };
        }
        public bool edit(cancha cancha)
        {
            using (Model1Container can = new Model1Container())
            {
                try
                {
                    int codigo_cancha = Convert.ToInt32(cancha.codigo_cancha);
                    cancha ca = can.canchaSet.Where(c => c.codigo_cancha == codigo_cancha).First();
                    ca.nombre_cancha = cancha.nombre_cancha;
                    ca.caracteristicas = cancha.caracteristicas;
                    can.canchaSet.Add(ca);
                    can.SaveChanges();
                    return true;
                }
                catch
                {
                    return false;
                }

            };
        }
        public bool delete(cancha cancha)
        {
            using (Model1Container can = new Model1Container())
            {
                try
                {
                    int codigo_cancha = Convert.ToInt32(cancha.codigo_cancha);
                    cancha ca = can.canchaSet.Single(c => c.codigo_cancha == codigo_cancha);
                    can.canchaSet.Remove(ca);
                    can.SaveChanges();

                    return true;
                }
                catch
                {
                    return false;
                }

            }
        }
    }
}
